# HOWTO Release Tezt

Decide on a Version Number X.Y.Z:
- breaking change => increment X and set Y and Z to 0
  - some breaking changes impact very niche use cases
    and may be considered "technically breaking but not really",
    in which case consider doing a minor release instead
  - big impactful changes that are not breaking may also justify major versions
- no breaking change but new features => increment Y and set Z to 0
- no breaking change, no new feature, only bug fixes => increment Z

Changelog:
- [ ] proof-read the changes for the Development Version section
- [ ] check if anything may be missing (use `git log` and compare)
- [ ] make sure breaking changes are in their own subsection
- [ ] remember that only changes that impact users need to be documented, not tests etc.
- [ ] rename section "Development Version" into "Version X.Y.Z"

Check Documentation:
- [ ] rebase the `pages` branch
- [ ] update `docs-index-top` to add the `<li>` for the new version
- [ ] run `VERSION=X.Y.Z make`
- [ ] check the result and commit it

Check Opam Package:
- [ ] check `tezt.opam` at the root of the repository, in particular dependencies
- [ ] test that it installs correctly with `opam pin .`
- [ ] after that don't forget to unpin with `opam pin remove tezt`

Tag:
- [ ] push a commit that updates the version number in `lib_core/version.ml`
  - remove `+dev` as well
- [ ] create an annotated Git tag using `git tag -a`
  - tag should be of the form `X.Y.Z`
- [ ] push this tag
- [ ] push a commit that adds `+dev` back in `lib_core/version.ml`

Publish Documentation:
- [ ] push the previously-rebased `pages` branch

Release on Opam:
- [ ] download archive with:
      `wget https://gitlab.com/nomadic-labs/tezt/-/archive/X.Y.Z/tezt-X.Y.Z.tar.bz2`
- [ ] compute MD5 and SHA512 hash of this tarball with `md5hash` and `sha512hash`
- [ ] fork https://github.com/ocaml/opam-repository/ if you don't have a fork already
- [ ] `git clone` this fork
- [ ] copy `tezt.opam` from the Tezt repository into the opam repository as:
  `packages/tezt/tezt.X.Y.Z/opam`
- [ ] run `diff` to compare with the `opam` file of the previous version
- [ ] notice that the `url` section is missing from the new version: add it
  - use the URL of the archive that you passed to `wget` earlier
  - use the checksums that you computed earlier with `md5hash` and `sha512hash`
- [ ] create a branch and commit this file
  - example commit message: `add tezt X.Y.Z`
- [ ] push this branch and go on GitHub to create a pull request
- [ ] wait for this pull request to be merged, or for any comment from the opam maintainer
  - you can also check the opam CI to react faster to any issue,
    if only by leaving a comment like "this test fails but it's ok"
- [ ] test with `opam update; install tezt.X.Y.Z`
