(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* SPDX-FileCopyrightText: 2024 Nomadic Labs <contact@nomadic-labs.com>      *)
(*                                                                           *)
(*****************************************************************************)

(** Running tasks on a set number of forked processes. *)

(** The main content of this module is:
    - a function {!add_task} to add tasks to a queue;
    - a function {!run} to run those tasks in separate worker processes;
    - a module {!Message} to send messages to and from workers;
    - a module {!Timer} to schedule delayed functions.

    The scheduler (i.e. {!run}) maintains a pool of workers.
    Workers receive tasks from the scheduler, execute them
    and send the result to the scheduler.
    Tasks can be given a limited amount of time to run.
    New tasks can be added (with {!add_task}) while the scheduler is running
    in response to events such as:
    - a task being started;
    - a task finishing (successfully or not);
    - a message being received from a worker (i.e. from a running task);
    - the task queue becoming empty.

    Examples of messages include:
    - logs: tasks can send log messages to the scheduler,
      so that the scheduler can write them in a single file with no interleaving;
    - global resource queries: workers can query a resource such as a free port number,
      and the scheduler can respond with this resource.

    Messages are meant to be used while a task is running.
    The return value of a task is also sent to the scheduler as a message,
    but this is handled transparently.

    Examples of use cases for this library include:
    - a test framework that wants to sandbox tests in parallel and in separate processes
      (e.g. to be able to recover from crashes, and to kill them if they take too long);
    - a build system that wants to compile multiple targets in parallel,
      and to add the reverse dependencies of a target as new tasks
      once this target is built (such as [make -j]). *)

(** {2 Contexts} *)

(** Message sending functions are supposed to be run from a specific context:
    - the worker process, which can send messages to and receive message from the scheduler;
    - or the scheduler process with a specific worker in mind,
      from which to receive from and to which to send to.

    Those message sending functions take a value of type {!worker_context}
    or {!scheduler_context} as proof.
    (There are ways to leak the contexts to other contexts but it would
    be a programming error.) *)

(** Values used by message sending functions meant to be called from a worker. *)
type worker_context

(** Values used by message sending functions meant to be called from the scheduler. *)
type scheduler_context

(** Get the current worker context.

    Returns [None] if not currently in a worker process. *)
val get_current_worker_context : unit -> worker_context option

(** {2 Messages} *)

module Message : sig
  (** Send and receive messages to and from workers. *)

  (** Messages are composed of:
      - a tag, that uniquely identifies the type of the message;
      - a dynamic value.

      Values are said to be "dynamic" because, while they carry information
      about their type at runtime, their type is lost at compilation time.
      To recover static type information, one must decode the value
      by matching with one or more expected tags.

      Ideally, we would have used an extensible GADT type to define tags.
      This would have been both more efficient
      (no need to encode values into the dynamic value type)
      and more convenient (no need to define encoding and decoding functions),
      while still allowing new types of messages to be defined by the user.
      Unfortunately, values of extensible types (GADT or not) interact badly
      with [Marshal]. *)

  (** Untyped values.

      Values are serialized and deserialized using [Marshal].
      Be careful with variables that are captured in [Closure]s.
      For instance, extensible types like exceptions can be serialized,
      but they cannot be [match]ed on after they are deserialized. *)
  type value =
    | Unit
    | Bool of bool
    | Char of char
    | Int of int
    | Int32 of int32
    | Int64 of int64
    | Float of float
    | String of string
    | Block of value array
    | Closure of (unit -> unit)

  (** Convert an untyped value to a human-readable string, for debugging. *)
  val show_value : value -> string

  (** Type descriptions for message values of type ['mv]. *)
  type 'mv typ

  (** A value could not be decoded.

      The first argument is the value that could not be decoded.
      The second argument is the name of the type that the value was expected to have,
      such as ["bool"] or ["string"]. *)
  exception Failed_to_decode of value * string

  (** Make a type description from an encoding and a decoding function.

      It is recommended to raise [Failed_to_decode] in [decode] if the value
      cannot be decoded for the expected type. *)
  val typ : encode:('mv -> value) -> decode:(value -> 'mv) -> 'mv typ

  (** Get the encoding function of a type description. *)
  val encode : 'mv typ -> 'mv -> value

  (** Get the decoding function of a type description. *)
  val decode : 'mv typ -> value -> 'mv

  (** The [unit] type description. *)
  val unit : unit typ

  (** The [bool] type description. *)
  val bool : bool typ

  (** The [char] type description. *)
  val char : char typ

  (** The [int] type description. *)
  val int : int typ

  (** The [int32] type description. *)
  val int32 : int32 typ

  (** The [int64] type description. *)
  val int64 : int64 typ

  (** The [float] type description. *)
  val float : float typ

  (** The [string] type description. *)
  val string : string typ

  (** The type description for [unit -> unit] closures. *)
  val closure : (unit -> unit) typ

  (** Message tags.

      The type parameter ['mv] is the type of message values before they are encoded
      and after they are decoded. *)
  type 'mv tag

  (** Register a new message tag.

      Usage: [register typ name]

      This returns a new tag with a unique internal identifier.
      This guarantees that messages with different tags are not mistaken
      for each other. Unless you register so many tags that you reach an integer overflow.

      The [name] of the tag is used by {!show}.
      Note that if you register a tag in the scheduler, workers that are already running
      will not know about it. They will still be able to handle those messages,
      but {!show} will print an integer instead of [name].
      Declaring tags in a worker is kind of pointless because messages with this tag
      will not be readable in the scheduler. *)
  val register : 'mv typ -> string -> 'mv tag

  (** Messages. *)
  type t

  (** Make a message from a tag and a (typed) value. *)
  val make : 'mv tag -> 'mv -> t

  (** Convert a message to a human-readable string, for debugging. *)
  val show : t -> string

  (** Match a message with a tag.

      Usage: [match_with message tag handler ~default]

      If [message] has tag [tag], the decoded message value is passed to [handler].
      Else, [default] is called. *)
  val match_with :
    t -> 'mv tag -> ('mv -> 'result) -> default:(unit -> 'result) -> 'result

  (** Cases for {!match_with_list}. *)
  type 'a case

  (** Make a case for {!match_with_list}

      Cases are composed of a [tag] and a [handler].
      The [handler] is called if the message that is being matched has this [tag]. *)
  val case : 'mv tag -> ('mv -> 'result) -> 'result case

  (** Same as {!match_with}, but for a list of tags.

      Example:
      {[
        match_with_list message
          [
            (case a @@ fun x -> ...);
            (case b @@ fun x -> ...);
          ]
          ~default: (fun x -> ...)
      ]} *)
  val match_with_list :
    t -> 'result case list -> default:(unit -> 'result) -> 'result

  (** Send a message from the scheduler to a worker.

      This is meant to be called from the scheduler.
      This is non-blocking.

      There is no [receive_from_worker] function.
      Instead, use an [on_message] event handler to receive messages from workers. *)
  val send_to_worker : scheduler_context -> 'mv tag -> 'mv -> unit

  (** Send a message from a worker to the scheduler.

      This is meant to be called from a worker.
      This is blocking, although it should usually return very quickly. *)
  val send_to_scheduler : worker_context -> 'mv tag -> 'mv -> unit

  (** Receive a message from the scheduler.

      This is meant to be called from a worker.
      This blocks until a message is available or end of file is received,
      in which case it returns [None]. *)
  val receive_from_scheduler : worker_context -> t option

  (** Same as {!receive_from_scheduler} but with a timeout, in seconds.

      Returns [None] if the timeout is reached, or end of file is received,
      before receiving a complete message. *)
  val receive_from_scheduler_with_timeout : worker_context -> float -> t option
end

(** {2 Timers} *)

module Timer : sig
  (** Call functions after a given amount of time. *)

  (** Timers trigger in the scheduler process,
      and only if they were created in the scheduler process.
      If a worker runs a scheduler itself,
      this worker does not inherit the timers from its parent scheduler. *)

  (** Timers. *)
  type t

  (** Create a timer.

      Usage: [on_delay delay f]

      This causes {!run} to trigger [f ()] after [delay] seconds.
      This also causes {!run} to not return until this timer has triggered,
      even if all tasks are done, unless the timer is {!cancel}ed. *)
  val on_delay : float -> (unit -> unit) -> t

  (** Cancel a timer.

      This removes the timer from the list of active timers.
      The function associated with the timer will not be triggered by {!run},
      and {!run} will not wait for the timer to occur.

      If the timer has already been triggered, this has no effect. *)
  val cancel : t -> unit

  (** Cancel all active timers. *)
  val cancel_all : unit -> unit
end

(** {2 Task Queue} *)

(** Add a task to the queue.

    Usage: [add_task typ execute]

    [typ] is a type description for values returned by [execute].
    When a worker is ready to execute this task, this worker will run [execute].
    Note that [execute] is serialized to the worker using [Marshal].
    If this closure captures some variables, those variables should thus be
    serializable using [Marshal].

    [add_task] can be called before {!run}, or while {!run} is running
    (i.e. from an event handler like [on_start], [on_message], [on_finish];
    or the [on_empty_queue] argument of {!run}).

    If [term_timeout] is specified, [sigterm] is sent to the worker
    if the task has not finished (successfully or not) after [term_timeout] seconds.
    [sigterm] defaults to [Sys.sigterm].

    If [kill_timeout] is specified but [term_timeout] is not, [SIGKILL] is sent to the worker
    if the task has not finished (successfully or not) after [kill_timeout] seconds.

    If both [term_timeout] and [kill_timeout] are specified, [sigterm] is sent first,
    and if the task is not willing to end gracefully [kill_timeout] seconds after
    [sigterm] was sent, [SIGKILL] is sent as well. Note that in that case,
    [kill_timeout] is relative to the time [sigterm] was sent, not to the time
    the task started.

    [on_start] is triggered when the task is sent to a worker.
    It takes a [scheduler_context] argument that allows to send a message to this worker,
    typically with additional information that was not known at the time the task
    was queued, such as a free port number that the worker can use.

    [on_message] is triggered for each message that is sent from the worker.
    It also takes a [scheduler_context] argument to be able to respond.

    [on_finish] is triggered with:
    - [Ok result] when the task returns successfully, in which case [result]
      is the return value of [execute];
    - [Error error_message] when the task fails, in which case [error_message]
      can be the result of [Printexc.to_string] (if [execute] raised an exception)
      or something else (e.g. if the worker died). *)
val add_task :
  ?sigterm:int ->
  ?term_timeout:float ->
  ?kill_timeout:float ->
  ?on_term_timeout:(unit -> unit) ->
  ?on_kill_timeout:(unit -> unit) ->
  ?on_start:(scheduler_context -> unit) ->
  ?on_message:(scheduler_context -> Message.t -> unit) ->
  ?on_finish:(('a, string) result -> unit) ->
  'a Message.typ ->
  (worker_context -> 'a) ->
  unit

(** Clear the queue of tasks.

    This has no effect on tasks that are already running,
    because they have been removed from the queue. *)
val clear : unit -> unit

(** {2 Main Loop} *)

(** Run tasks until the queue is empty.

    [on_empty_queue] is called when a worker is available and the task queue is empty.
    It can in particular use {!add_task} to fill the queue.

    [on_message] is called when a worker emits a message while not executing a task.
    This can happen in particular if you use [at_exit].
    Messages received from a worker which is running a task are passed to the [on_message]
    of the corresponding {!add_task} call instead.

    When a worker exits:
    - if it is running a task, the task fails (its [on_finish] is triggered with [Error]);
    - if it is not running a task and the queue is not empty, or if the exit code is not 0,
      [on_unexpected_worker_exit] is called so that you can emit a warning.

    If [worker_idle_timeout] is specified, workers stop if they are not given any task
    after [worker_idle_timeout] seconds of doing nothing. This can be useful to prevent
    workers from running forever, although in general they should detect that the scheduler
    is dead by receiving end of file while trying to receive their next task.

    If [worker_kill_timeout] is specified, send [SIGKILL] to workers if they are still running
    [worker_kill_timeout] seconds after they were told to stop.
    This only applies when they were told to stop because the task queue is empty.
    When this happens, [on_worker_kill_timeout] is called.

    [fork] is supposed to be [Unix.fork].
    But if tasks may use Lwt, it should be [Lwt_unix.fork] instead.
    You can also modify fork to run some code on each fork, for instance to initialize
    some global variables when a worker starts.

    The last argument is the maximum number of tasks to run in parallel.

    This function is blocking.
    It returns once:
    - no task is currently running;
    - the queue is empty (unless you called {!stop});
    - and no timer is currently active.
    In particular, it returns immediately if you never called {!add_task}
    and {!Timer.on_delay}. *)
val run :
  ?worker_idle_timeout:float ->
  ?worker_kill_timeout:float ->
  ?on_worker_kill_timeout:(unit -> unit) ->
  ?on_empty_queue:(unit -> unit) ->
  ?on_message:(Message.t -> unit) ->
  ?on_unexpected_worker_exit:(Unix.process_status -> unit) ->
  fork:(unit -> int) ->
  int ->
  unit

(** Stop the current {!run}.

    This function is meant to be called from the event handlers ([on_] functions) of tasks.
    It does nothing if {!run} is not currently running.

    This function is not blocking.
    It causes {!run} to stop starting new tasks.
    It also causes {!run} to consider that all current tasks are passed their [term_timeout],
    even if they do not actually have such a timeout,
    except that [on_term_timeout] is not triggered.
    In other words, all workers receive [SIGTERM] if they didn't already.

    Calling {!add_task} still adds tasks to the queue
    but they will not be started unless you call {!run} again.

    This function does not cancel timers. *)
val stop : unit -> unit

(** {2 Miscellaneous} *)

(** Convert a process status to a human-readable string.

    Example results:
    - ["exited with code 0"]
    - ["was killed by SIGTERM"]
    - ["was stopped by unknown signal (-100)"] *)
val show_process_status : Unix.process_status -> string
