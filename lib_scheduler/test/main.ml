(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* SPDX-FileCopyrightText: 2024 Nomadic Labs <contact@nomadic-labs.com>      *)
(*                                                                           *)
(*****************************************************************************)

open Scheduler

let () = Printexc.register_printer @@ function Failure s -> Some s | _ -> None

let sf = Printf.sprintf

let start_time = Unix.gettimeofday ()

let log_with_timestamp message =
  Printf.printf "[%.3f] %s\n%!" (Unix.gettimeofday () -. start_time) message

let log x = Printf.ksprintf log_with_timestamp x

let msg_log = Message.(register string) "Log"

let log_string_in_scheduler s =
  match get_current_worker_context () with
  | None -> failwith "tried to log in scheduler while already in scheduler"
  | Some ctx -> Message.send_to_scheduler ctx msg_log s

let log_in_scheduler x = Printf.ksprintf log_string_in_scheduler x

let start_count = ref 0

let finish_count = ref 0

let task ?term_timeout ?kill_timeout ?(on_start = fun _ -> ())
    ?(on_message = fun _ _ -> ()) ?(on_finish = fun _ -> ()) ?expect =
  add_task
    ?term_timeout
    ?kill_timeout
    ~on_start:(fun ctx ->
      incr start_count ;
      on_start ctx)
    ~on_message:(fun ctx message ->
      log "[message] %s" (Message.show message) ;
      on_message ctx message)
    ~on_finish:(fun result ->
      incr finish_count ;
      (match expect with
      | None -> ()
      | Some expect -> (
          match (result, expect) with
          | Ok i, Ok j ->
              if i = j then log "Task returned value %d as expected" i
              else failwith @@ sf "Task returned value %d instead of %d" i j
          | Ok i, Error e ->
              failwith
              @@ sf "Task returned value %d instead of failing with %S" i e
          | Error e, Ok i ->
              failwith
              @@ sf "Task failed with %S instead of returning value %d" e i
          | Error e, Error f ->
              if e = f then log "Task failed with %S as expected" e
              else failwith @@ sf "Task failed with %S instead of %S" e f)) ;
      on_finish result)
    Message.int

let run ?expect ?on_empty_queue n =
  start_count := 0 ;
  finish_count := 0 ;
  run ?on_empty_queue ~fork:Unix.fork n ;
  match expect with
  | None -> ()
  | Some expect ->
      if !start_count <> expect then
        failwith @@ sf "expected start_count = %d, got %d" expect !start_count ;
      if !finish_count <> expect then
        failwith @@ sf "expected finish_count = %d, got %d" expect !finish_count

let () =
  log "Run no task." ;
  run 1 ~expect:0

let () =
  log "Run one task." ;
  task
    (fun _ ->
      log "Running task..." ;
      17)
    ~expect:(Ok 17) ;
  run 1 ~expect:1

let () =
  log "Run one task that fails." ;
  task
    (fun _ ->
      log "Running failing task..." ;
      failwith "failed")
    ~expect:(Error "failed") ;
  run 1 ~expect:1

let () =
  log "Run two tasks in a single worker." ;
  task
    (fun _ ->
      log "Running task 1..." ;
      18)
    ~expect:(Ok 18) ;
  task
    (fun _ ->
      log "Running task 2..." ;
      19)
    ~expect:(Ok 19) ;
  run 1 ~expect:2

let () =
  log "Run one task in two workers." ;
  task
    (fun _ ->
      log "Running task..." ;
      20)
    ~expect:(Ok 20) ;
  run 2 ~expect:1

let () =
  log "Run two tasks in two workers." ;
  task
    (fun _ ->
      log "Running task 1..." ;
      21)
    ~expect:(Ok 21) ;
  task
    (fun _ ->
      log "Running task 2..." ;
      22)
    ~expect:(Ok 22) ;
  run 2 ~expect:2

let () =
  log "Run three tasks in two workers." ;
  task
    (fun _ ->
      log "Running task 1..." ;
      23)
    ~expect:(Ok 23) ;
  task
    (fun _ ->
      log "Running task 2..." ;
      24)
    ~expect:(Ok 24) ;
  task
    (fun _ ->
      log "Running task 3..." ;
      25)
    ~expect:(Ok 25) ;
  run 2 ~expect:3

let () =
  log "Run a scheduler inside a worker." ;
  ( task @@ fun _ ->
    log "Running task 1..." ;
    task
      (fun _ ->
        log "Running task 1.1..." ;
        100)
      ~expect:(Ok 100) ;
    task
      (fun _ ->
        log "Running task 1.2..." ;
        101)
      ~expect:(Ok 101) ;
    task
      (fun _ ->
        log "Running task 1.3..." ;
        102)
      ~expect:(Ok 102) ;
    run 2 ~expect:3 ;
    26 ) ;
  task
    (fun _ ->
      log "Running task 2..." ;
      27)
    ~expect:(Ok 27) ;
  task
    (fun _ ->
      log "Running task 3..." ;
      28)
    ~expect:(Ok 28) ;
  run 2 ~expect:3

let () =
  log "Exit the worker prematurely." ;
  task
    (fun _ ->
      log "Running task 1..." ;
      exit 17)
    ~expect:(Error "worker exited with code 17") ;
  task
    (fun _ ->
      log "Running task 2..." ;
      exit 18)
    ~expect:(Error "worker exited with code 18") ;
  task
    (fun _ ->
      log "Running task 3..." ;
      Unix.sleepf 0.1 ;
      29)
    ~expect:(Ok 29) ;
  task
    (fun _ ->
      log "Running task 4..." ;
      Unix.sleepf 0.1 ;
      30)
    ~expect:(Ok 30) ;
  task
    (fun _ ->
      log "Running task 5..." ;
      Unix.sleepf 0.1 ;
      31)
    ~expect:(Ok 31) ;
  run 2 ~expect:5

let long_task ?term_timeout ?kill_timeout ?(ignore_sigterm = false) ?expect () =
  task ?term_timeout ?kill_timeout ?expect @@ fun _ ->
  log "Running long task..." ;
  if ignore_sigterm then Sys.(set_signal sigterm) (Signal_handle (fun _ -> ())) ;
  Unix.sleepf 0.5 ;
  failwith "I should have been killed by now"

let () =
  log "Have a task reach its timeout (SIGTERM)." ;
  long_task ~term_timeout:0.1 () ~expect:(Error "worker was killed by SIGTERM") ;
  run 1 ~expect:1

let () =
  log "Have a task reach its timeout (SIGTERM + SIGKILL)." ;
  long_task
    ~term_timeout:0.1
    ~kill_timeout:0.1
    ()
    ~expect:(Error "worker was killed by SIGTERM") ;
  run 1 ~expect:1

let () =
  log "Have a task reach its timeout (SIGKILL)." ;
  long_task ~kill_timeout:0.1 () ~expect:(Error "worker was killed by SIGKILL") ;
  run 1 ~expect:1

let () =
  log "Have a task reach its timeout (SIGTERM, ignore)." ;
  long_task
    ~term_timeout:0.1
    ~ignore_sigterm:true
    ()
    ~expect:(Error "I should have been killed by now") ;
  run 1 ~expect:1

let () =
  log "Have a task reach its timeout (SIGTERM + SIGKILL, ignore)." ;
  long_task
    ~term_timeout:0.1
    ~kill_timeout:0.1
    ~ignore_sigterm:true
    ()
    ~expect:(Error "worker was killed by SIGKILL") ;
  run 1 ~expect:1

let () =
  log "Have a task reach its timeout (SIGKILL, ignore)." ;
  long_task
    ~kill_timeout:0.1
    ~ignore_sigterm:true
    ()
    ~expect:(Error "worker was killed by SIGKILL") ;
  run 1 ~expect:1

let () =
  log "Have the scheduler log for tasks." ;
  let logs = ref [] in
  let on_message _ message =
    Message.match_with message msg_log ~default:(fun () -> ()) @@ fun text ->
    logs := text :: !logs
  in
  for i = 1 to 10 do
    task ~on_message @@ fun _ ->
    log_in_scheduler "This is task %d." i ;
    log_in_scheduler "I'm logging stuff." ;
    i
  done ;
  run 3 ;
  let logs = List.sort String.compare !logs in
  let expected =
    [
      "I'm logging stuff.";
      "I'm logging stuff.";
      "I'm logging stuff.";
      "I'm logging stuff.";
      "I'm logging stuff.";
      "I'm logging stuff.";
      "I'm logging stuff.";
      "I'm logging stuff.";
      "I'm logging stuff.";
      "I'm logging stuff.";
      "This is task 1.";
      "This is task 10.";
      "This is task 2.";
      "This is task 3.";
      "This is task 4.";
      "This is task 5.";
      "This is task 6.";
      "This is task 7.";
      "This is task 8.";
      "This is task 9.";
    ]
  in
  if logs <> expected then (
    log "Sorted logs:" ;
    List.iter (log "- %S") logs ;
    failwith "didn't receive the expected logs")

let () =
  log "Have tasks ask the scheduler for resources." ;
  let next_resource = ref 0 in
  let msg_get_resource = Message.(register unit) "Get_resource" in
  let msg_resource = Message.(register int) "Resource" in
  let ok_count = ref 0 in
  let timeout_count = ref 0 in
  let on_finish = function
    | Ok _ -> incr ok_count
    | Error "timeout while waiting for resource" -> incr timeout_count
    | Error s -> failwith @@ sf "task failed with %S" s
  in
  let task n =
    let on_message ctx message =
      Message.match_with message msg_get_resource ~default:(fun () -> ())
      @@ fun () ->
      let resource = !next_resource in
      (* Don't answer after a while to test the timeout. *)
      if resource < 8 then (
        incr next_resource ;
        Message.send_to_worker ctx msg_resource resource)
    in
    task ~on_message ~on_finish @@ fun ctx ->
    log "Running task %d..." n ;
    Message.send_to_scheduler ctx msg_get_resource () ;
    let response = Message.receive_from_scheduler_with_timeout ctx 0.1 in
    let resource =
      match response with
      | None -> failwith "timeout while waiting for resource"
      | Some response ->
          Message.match_with
            response
            msg_resource
            ~default:(fun () -> failwith "unexpected response")
            Fun.id
    in
    log "Received resource: %d" resource ;
    1000 + resource
  in
  for i = 1 to 10 do
    task i
  done ;
  run 4 ;
  log "ok_count = %d, timeout_count = %d" !ok_count !timeout_count ;
  if !ok_count <> 8 then failwith "expected ok_count = 8" ;
  if !timeout_count <> 2 then failwith "expected timeout_count = 2"

let () =
  log "Stop the scheduler while it is running." ;
  task
    ~on_finish:(fun _ ->
      log "Stop." ;
      stop ())
    (fun _ ->
      log "Running task 1..." ;
      50) ;
  ( task ~expect:(Error "worker was killed by SIGTERM") @@ fun _ ->
    Unix.sleepf 0.1 ;
    failwith "I am task 2 and I am supposed to have been killed with SIGTERM."
  ) ;
  ( task ~expect:(Error "worker was killed by SIGTERM") @@ fun _ ->
    Unix.sleepf 0.1 ;
    failwith "I am task 3 and I am supposed to have been killed with SIGTERM."
  ) ;
  task (fun _ -> failwith "I am task 4 and I am not supposed to be started") ;
  run 3 ~expect:3 ;
  (* Clear the queue of task because otherwise the next [run] would run task 4. *)
  clear ()

let () =
  log "Re-filling the queue." ;
  let on_empty_queue =
    let n = ref 1 in
    fun () ->
      if !n <= 3 then (
        let batch = !n in
        log "Refill." ;
        for i = 1 to 2 do
          task (fun _ ->
              log "Running task %d.%d" batch i ;
              (batch * 100) + i)
        done ;
        incr n)
  in
  run ~on_empty_queue 2 ~expect:6

let () =
  log "A task that creates other tasks." ;
  let msg_push = Message.(register float) "Push" in
  let on_message _ message =
    Message.match_with message msg_push ~default:(fun () -> ()) @@ fun x ->
    task @@ fun _ ->
    log "Running task %g" x ;
    int_of_float x
  in
  ( task ~on_message @@ fun ctx ->
    Message.send_to_scheduler ctx msg_push 1.1 ;
    Message.send_to_scheduler ctx msg_push 2.2 ;
    Message.send_to_scheduler ctx msg_push 3.3 ;
    Message.send_to_scheduler ctx msg_push 4.4 ;
    Message.send_to_scheduler ctx msg_push 5.5 ;
    0 ) ;
  run 3 ~expect:6

let () =
  log "Create a timer and run with no task." ;
  let triggered = ref false in
  let _ =
    Timer.on_delay 0.1 (fun () ->
        triggered := true ;
        log "It's time!")
  in
  run 1 ~expect:0 ;
  if not !triggered then failwith "timer did not trigger"

let () =
  log
    "Task that emits a message to create a timer that will trigger more tasks." ;
  let msg_push = Message.(register int) "Push" in
  let on_message _ message =
    Message.match_with message msg_push ~default:(fun () -> ()) @@ fun x ->
    ignore @@ Timer.on_delay 0.05
    @@ fun () ->
    task @@ fun _ ->
    log "Running subtask %d" x ;
    x
  in
  ( task ~on_message @@ fun ctx ->
    Unix.sleepf 0.1 ;
    Message.send_to_scheduler ctx msg_push 1 ;
    Unix.sleepf 0.1 ;
    Message.send_to_scheduler ctx msg_push 2 ;
    0 ) ;
  run 2 ~expect:3

let () =
  log "Create a timer and another that cancels the first." ;
  let timer =
    Timer.on_delay 1. @@ fun () -> failwith "I was supposed to be canceled"
  in
  let _ =
    Timer.on_delay 0.1 @@ fun () ->
    log "Cancel timer." ;
    Timer.cancel timer
  in
  run 1 ~expect:0

let () =
  log
    "Create a task and a timer that regularly logs until the task is finished." ;
  let timer = ref None in
  let beeps = ref 0 in
  let rec on_start ctx =
    timer :=
      Some
        ( Timer.on_delay 0.06 @@ fun () ->
          incr beeps ;
          log "Beep!" ;
          on_start ctx )
  in
  let on_finish _ =
    match !timer with
    | None -> failwith "no active timer to cancel"
    | Some timer -> Timer.cancel timer
  in
  task ~on_start ~on_finish (fun _ ->
      Unix.sleepf 0.2 ;
      0) ;
  run 1 ;
  if !beeps <> 3 then
    failwith @@ sf "expected to have beeped 3 times, got %d" !beeps
