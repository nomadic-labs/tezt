/*****************************************************************************/
/*                                                                           */
/* SPDX-License-Identifier: MIT                                              */
/* Copyright (c) 2023 Nomadic Labs <contact@nomadic-labs.com>                */
/*                                                                           */
/*****************************************************************************/

%{

open Base
open TSL_AST

let rex s =
  try
    rex s
  with Re.Perl.Parse_error ->
    failwith ("invalid regular expression: " ^ s)

let string_var = function
  | "file" -> File
  | "title" -> Title
  | var -> failwith ("unknown value: " ^ var)

%}

%token <string> STRING SLASH_STRING
%token TRUE FALSE EQUAL NOT_EQUAL MATCHES NOT_MATCHES LPAR RPAR NOT OR AND EOF

%left OR
%left AND
%nonassoc EQUAL MATCHES NOT_EQUAL NOT_MATCHES
%nonassoc NOT

%type <TSL_AST.t> expression
%start expression
%%

expression:
| expr EOF
  { $1 }

expr:
| TRUE
  { True }
| FALSE
  { False }
| expr AND expr
  { And ($1, $3) }
| expr OR expr
  { Or ($1, $3) }
| NOT expr
  { Not $2 }
| NOT
  { Has_tag "not" }
| STRING
  { Has_tag $1 }
| SLASH_STRING
  { Not (Has_tag $1) }
| STRING EQUAL STRING
  { String_predicate (string_var $1, Is $3) }
| STRING NOT_EQUAL STRING
  { Not (String_predicate (string_var $1, Is $3)) }
| STRING MATCHES STRING
  { String_predicate (string_var $1, Matches (rex $3)) }
| STRING NOT_MATCHES STRING
  { Not (String_predicate (string_var $1, Matches (rex $3))) }
| LPAR expr RPAR
  { $2 }
