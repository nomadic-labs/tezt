(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020-2023 Nomadic Labs <contact@nomadic-labs.com>           *)
(* Copyright (c) 2020 Metastate AG <hello@metastate.dev>                     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Command-line interface.

    When this module is loaded, it parses command line options
    unconditionally as a side-effect, using the Clap library.

    In general, you probably don't need to use this module to define tests.
    See "Defining Custom Arguments". *)

(** {2 Defining Custom Arguments} *)

(** Tezt uses the Clap library to define its command-line arguments.
    [Clap.close], which signals the end of command-line argument definitions,
    is called by [Test.run].

    This means that you can define your own arguments with [Clap] before calling
    [Test.run]. In general, you should avoid using their values before [Clap.close],
    so you should only use them inside tests (i.e. from functions given to [Test.register]).
    There are exceptions to this rule (see the
    {{: https://github.com/rbardou/clap/blob/master/README.md }README of Clap}).

    You should avoid defining unnamed arguments (i.e. arguments that are not prefixed
    by a dash), because those are already used by Tezt for tags. *)

module Options : sig
  (** General command-line arguments. *)

  (** What to do with temporary files after the test is finished. *)
  type temporary_file_mode = Delete | Delete_if_successful | Keep

  (** [--keep-temp], [--delete-temp], [--delete-temp-if-success] *)
  val temporary_file_mode : temporary_file_mode

  (** [--keep-going] *)
  val keep_going : bool

  (** [--global-timeout] *)
  val global_timeout : float option

  (** [--test-timeout] *)
  val test_timeout : float option

  (** [--cleanup-timeout] *)
  val cleanup_timeout : float

  (** [--warn-after-timeout] *)
  val warn_after_timeout : float

  (** [--retry] *)
  val retry : int

  (** [--reset-regressions] *)
  val reset_regressions : bool

  (** What to do with unknown regression files. *)
  type on_unknown_regression_files_mode = Warn | Ignore | Fail | Delete

  (** [--on-unknown-regression-files] *)
  val on_unknown_regression_files_mode : on_unknown_regression_files_mode

  (** How many times to loop. *)
  type loop_mode = Infinite | Count of int

  (** [--loop], [--loop-count] *)
  val loop_mode : loop_mode

  (** [--resume-file] *)
  val resume_file : string option

  (** [--resume] *)
  val resume : bool

  (** [--job-count] *)
  val job_count : int

  (** [--seed] *)
  val seed : int option
end

module Logs : sig
  (** Command-line arguments that control logging. *)

  (** [--color], [--no-color] *)
  val color : bool

  (** [--log-timestamp], [--no-log-timestamp] *)
  val timestamp : bool

  (** [--log-prefix], [--no-log-prefix] *)
  val prefix : bool

  (** Log levels for standard output.

      The list below is sorted from the most quiet level to the most verbose level.

      - Absolutely no log has log level [Quiet].
        In other words, setting log level [Quiet] inhibits all logs.

      - [Error] logs are about errors which imply that the current test failed.
        This includes messages given to [Test.fail] and uncaught exceptions.

      - [Warn] logs are about errors that do not cause the current test to fail.
        This includes failure to clean up temporary files, for instance.

      - [Report] logs are informational messages that report the result of the current test.
        They tell the user whether the test was successful or not.
        They may also include information about how to re-run the test.

      - [Info] logs are informational messages that summarize what the test is doing.
        They tell the user that a particular milestone was reached.
        In tests, it is a good idea to log [Info] messages at significant checkpoints.

      - [Debug] logs give more details about exactly what is happening.
        They include external process outputs, exit codes, and signals which are sent.

      Additionally, some flags such as [--commands] and [--list] cause some information
      to be printed unconditionally, even with [--quiet]. Such kind of output is not
      considered to be log messages. *)
  type level = Quiet | Error | Warn | Report | Info | Debug

  (** [--log-level], [--verbose], [--quiet], [--info] *)
  val level : level

  (** [--log-file] *)
  val file : string option

  (** [--log-buffer-size] *)
  val buffer_size : int

  (** [--log-worker-id] *)
  val worker_id : bool

  (** [--commands] *)
  val commands : bool
end

module Reports : sig
  (** Command-line arguments that control reporting. *)

  (** [--time] *)
  val time : bool

  (** [--record] *)
  val record : string option

  (** [--from-record] *)
  val from_records : string list

  (** [--junit] *)
  val junit : string option
end

module Commands : sig
  (** Command-line arguments that cause Tezt to do something else than running tests. *)

  (** What to do. *)
  type command = Run | List | List_tsv | Suggest_jobs | Version

  (** [--list], [--list-tsv], [--suggest-jobs] *)
  val command : command
end

module Selecting_tests : sig
  (** Command-line arguments that control the set of tests to run. *)

  (** [--file] *)
  val files_to_run : string list

  (** [--not-file] *)
  val files_not_to_run : string list

  (** [--match] *)
  val patterns_to_run : Base.rex list

  (** [--not-match] *)
  val patterns_not_to_run : Base.rex list

  (** [--title], [--test] *)
  val tests_to_run : string list

  (** [--not-title], [--not-test] *)
  val tests_not_to_run : string list

  (** [--job] *)
  val job : (int * int) option

  (** [--skip] *)
  val skip : int

  (** [--only] *)
  val only : int option

  (** Unnamed arguments are conditions expressed in the Test Selection Language.

      This returns the conjunction of all such conditions.

      This is a function because it should be called just before [Clap.close],
      to follow Clap's rule that named arguments must be defined before unnamed ones
      (see Clap's readme). *)
  val tsl_expression : unit -> TSL_AST.t

  (** How to behave if the list of selected tests is empty. *)
  type on_empty_test_list = Ignore | Warn | Fail

  (** [--on-empty-test-list] *)
  val on_empty_test_list : on_empty_test_list
end

(** {2 Defining Custom Arguments (Deprecated Method)} *)

(** The functions below are deprecated.
    Instead, you can define arguments using Clap directly
    (see "Defining Custom Arguments"). *)

(** Get the value for a parameter specified with [--test-arg].

    Usage: [get parse parameter]

    If [--test-arg parameter=value] was specified on the command-line,
    this calls [parse] on [value]. If [parse] returns [None], this fails.
    If [parse] returns [Some x], this returns [x].

    If no value for [parameter] was specified on the command-line,
    this returns [default] if [default] was specified. Else, this fails.

    It is recommended to make it so that specifying parameters with [--test-arg]
    is not mandatory for everyday use. This means it is recommended to always
    give default values, and that those default values should be suitable
    for typical test runs. For parameters that can take a small number of values,
    it is usually better to register multiple tests, one for each possible value,
    and to use tags to select from the command-line.

    @raise Failure if [parse] returns [None] or if [parameter] was not
    specified on the command-line using [--test-arg] and no [default]
    value was provided. *)
val get : ?default:'a -> (string -> 'a option) -> string -> 'a

(** Same as [get parse parameter] but return [None] if [parameter] is absent. *)
val get_opt : (string -> 'a option) -> string -> 'a option

(** Same as [get bool_of_string_opt]. *)
val get_bool : ?default:bool -> string -> bool

(** Same as [get_opt bool_of_string_opt]. *)
val get_bool_opt : string -> bool option

(** Same as [get int_of_string_opt]. *)
val get_int : ?default:int -> string -> int

(** Same as [get_opt int_of_string_opt]. *)
val get_int_opt : string -> int option

(** Same as [get float_of_string_opt]. *)
val get_float : ?default:float -> string -> float

(** Same as [get_opt float_of_string_opt]. *)
val get_float_opt : string -> float option

(** Same as [get (fun x -> Some x)]. *)
val get_string : ?default:string -> string -> string

(** Same as [get_opt (fun x -> Some x)]. *)
val get_string_opt : string -> string option
