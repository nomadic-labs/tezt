(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020-2022 Nomadic Labs <contact@nomadic-labs.com>           *)
(* Copyright (c) 2020 Metastate AG <hello@metastate.dev>                     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Process outputs that can be duplicated. *)

(** This module is experimental. *)

(** An [Echo.t] represents the standard output or standard error output of a process.
    Those outputs are duplicated: one copy is automatically logged,
    the other goes into [lwt_channel] so that the user can read it.
    (The logging part is handled by the [Process] module, not [Echo].)

    [queue] is the bytes that have been received from the process, but that
    have not yet been read by the user of this module. Those bytes are
    split into chunks ([string]s). Those chunks are never empty.

    Strings of [queue] are references so that we can replace them when we
    read them partially. If efficiency becomes a concern, we could store
    a reference to an offset to avoid a call to String.sub.

    [pending] is a list of [Lwt_unix] notifications. *)
type t = {
  queue : string ref Queue.t;
  mutable lwt_channel : Lwt_io.input_channel option;
  mutable closed : bool;
  mutable pending : int list;
}

(** Send all [pending] notifications and clear [pending]. *)
val wake_up : t -> unit

(** Push a string into the [queue] of an echo, if the string is not empty. *)
val push : t -> string -> unit

(** Set [closed] to [true] and call [wake_up_echo]. *)
val close : t -> unit

(** Create an echo. *)
val create : unit -> t

(** Get the [lwt_channel] of an echo. *)
val get_lwt_channel : t -> Lwt_io.input_channel
