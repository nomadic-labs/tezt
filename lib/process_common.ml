(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020-2022 Nomadic Labs <contact@nomadic-labs.com>           *)
(* Copyright (c) 2020 Metastate AG <hello@metastate.dev>                     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Base

let show_signal code =
  if code = Sys.sigabrt then "SIGABRT"
  else if code = Sys.sigalrm then "SIGALRM"
  else if code = Sys.sigfpe then "SIGFPE"
  else if code = Sys.sighup then "SIGHUP"
  else if code = Sys.sigill then "SIGILL"
  else if code = Sys.sigint then "SIGINT"
  else if code = Sys.sigkill then "SIGKILL"
  else if code = Sys.sigpipe then "SIGPIPE"
  else if code = Sys.sigquit then "SIGQUIT"
  else if code = Sys.sigsegv then "SIGSEGV"
  else if code = Sys.sigterm then "SIGTERM"
  else if code = Sys.sigusr1 then "SIGUSR1"
  else if code = Sys.sigusr2 then "SIGUSR2"
  else if code = Sys.sigchld then "SIGCHLD"
  else if code = Sys.sigcont then "SIGCONT"
  else if code = Sys.sigstop then "SIGSTOP"
  else if code = Sys.sigtstp then "SIGTSTP"
  else if code = Sys.sigttin then "SIGTTIN"
  else if code = Sys.sigttou then "SIGTTOU"
  else if code = Sys.sigvtalrm then "SIGVTALRM"
  else if code = Sys.sigprof then "SIGPROF"
  else if code = Sys.sigbus then "SIGBUS"
  else if code = Sys.sigpoll then "SIGPOLL"
  else if code = Sys.sigsys then "SIGSYS"
  else if code = Sys.sigtrap then "SIGTRAP"
  else if code = Sys.sigurg then "SIGURG"
  else if code = Sys.sigxcpu then "SIGXCPU"
  else if code = Sys.sigxfsz then "SIGXFSZ"
  else string_of_int code

let parse_current_environment : unit -> string String_map.t =
 fun () ->
  let parse_env_kv key_value : (string * string) option =
    String.index_opt key_value '='
    |> Option.map (fun i ->
           ( Re.Str.string_before key_value i,
             Re.Str.string_after key_value (i + 1) ))
  in
  Unix.environment () |> Array.to_seq
  |> Seq.filter_map parse_env_kv
  |> String_map.of_seq
