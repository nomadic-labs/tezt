(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(*****************************************************************************)

open Tezt_core
open Base

let parse string =
  match TSL.parse string with
  | None -> Test.fail "%S does not parse" string
  | Some tsl -> tsl

let () =
  Test.register
    ~__FILE__
    ~title:"TSL parser: positive cases"
    ~tags:["tsl"; "parser"; "positive"]
  @@ fun () ->
  let check string expected =
    let result = parse string in
    if result <> expected then
      Test.fail
        "%S does not parse as expected but as: %s"
        string
        (TSL.show result)
  in
  (* Base expressions. *)
  check "true" True ;
  check "false" False ;
  check "file = a/b/c.ml" (String_predicate (File, Is "a/b/c.ml")) ;
  check
    "title <> hello_world.mll"
    (Not (String_predicate (Title, Is "hello_world.mll"))) ;
  check "title =~ \"/c.ml$\"" (String_predicate (Title, Matches (rex "/c.ml$"))) ;
  check
    "file =~! nope-not-this"
    (Not (String_predicate (File, Matches (rex "nope-not-this")))) ;
  check "hello" (Has_tag "hello") ;
  check "\"hello\"" (Has_tag "hello") ;
  check "\"he\\\\l\\\"lo\"" (Has_tag "he\\l\"lo") ;
  (* Operator: negation. *)
  check "not true" (Not True) ;
  check "not false" (Not False) ;
  check "/tag" (Not (Has_tag "tag")) ;
  check "not tag" (Not (Has_tag "tag")) ;
  check "not f.ml" (Not (Has_tag "f.ml")) ;
  check "not (file = f.ml)" (Not (String_predicate (File, Is "f.ml"))) ;
  (* Operators: conjunction and disjunction. *)
  check "true && false" (And (True, False)) ;
  check "true || false" (Or (True, False)) ;
  check
    "tag || file = truc.ml || title = \"some title\""
    (Or
       ( Or (Has_tag "tag", String_predicate (File, Is "truc.ml")),
         String_predicate (Title, Is "some title") )) ;
  check
    "tag && file = truc.ml && title = \"some title\""
    (And
       ( And (Has_tag "tag", String_predicate (File, Is "truc.ml")),
         String_predicate (Title, Is "some title") )) ;
  (* Operator precedence and parentheses. *)
  check "not file = f.ml" (Not (String_predicate (File, Is "f.ml"))) ;
  check "not (file = f.ml)" (Not (String_predicate (File, Is "f.ml"))) ;
  check "true && false || tag" (Or (And (True, False), Has_tag "tag")) ;
  check "true && (false || tag)" (And (True, Or (False, Has_tag "tag"))) ;
  check "true || false && tag" (Or (True, And (False, Has_tag "tag"))) ;
  check "(true || false) && tag" (And (Or (True, False), Has_tag "tag")) ;
  check
    "file = f && not (title = t) || tag && /notag"
    (Or
       ( And
           ( String_predicate (File, Is "f"),
             Not (String_predicate (Title, Is "t")) ),
         And (Has_tag "tag", Not (Has_tag "notag")) )) ;
  check
    "file = f && not (title = t || tag) && /notag"
    (And
       ( And
           ( String_predicate (File, Is "f"),
             Not (Or (String_predicate (Title, Is "t"), Has_tag "tag")) ),
         Not (Has_tag "notag") )) ;
  unit

let () =
  Test.register
    ~__FILE__
    ~title:"TSL parser: negative cases"
    ~tags:["tsl"; "parser"; "negative"]
  @@ fun () ->
  let check string =
    match TSL.parse string with
    | None -> ()
    | Some result ->
        Test.fail "%S unexpectedly parses as: %s" string (TSL.show result)
  in
  check "" ;
  check "/" ;
  check "\"" ;
  check "\"hello" ;
  check "(" ;
  check ")" ;
  check "\000" ;
  check "a &&" ;
  check "|| b" ;
  check "a && || c" ;
  check "/(a || b)" ;
  check "/(a)" ;
  unit

let () =
  Test.register
    ~__FILE__
    ~title:"TSL parser: pretty-printing"
    ~tags:["tsl"; "show"]
  @@ fun () ->
  let check string =
    let tsl = parse string in
    let shown = TSL.show tsl in
    if shown <> string then Test.fail "%S is shown as %S" string shown
  in
  (* Base expressions. *)
  check "true" ;
  check "false" ;
  check "file = f.ml" ;
  check "title = truc" ;
  check "title = \"truc machin\"" ;
  check "file =~ abc" ;
  check "file =~ \"[abc]+$\"" ;
  check "tag" ;
  check "\"string with \\\\ symbols \\\" in it\"" ;
  (* Operator: negation. *)
  check "not (file = f.ml)" ;
  check "not (title =~ f.ml)" ;
  check "/tag" ;
  (* Operators: conjunction and disjunction. *)
  check "true && false" ;
  check "true || false" ;
  check "tag || file = truc.ml || title = \"some title\"" ;
  check "tag && file = truc.ml && title = \"some title\"" ;
  (* Operator precedence and parentheses. *)
  check "true && false || tag" ;
  check "true && (false || tag)" ;
  check "true || false && tag" ;
  check "(true || false) && tag" ;
  check "file = f && not (title = t) || tag && /notag" ;
  check "file = f && not (title = t || tag) && /notag" ;
  unit

let () =
  Test.register ~__FILE__ ~title:"TSL: evaluation" ~tags:["tsl"; "evaluation"]
  @@ fun () ->
  let env : TSL.env =
    {file = "/some/file.ml"; title = "some title"; tags = ["some"; "tags"]}
  in
  let check string expected =
    let tsl = parse string in
    let result = TSL.eval env tsl in
    if result <> expected then
      Test.fail "%S evaluates to %b instead of %b" string result expected
  in
  (* Base expressions. *)
  check "true" true ;
  check "false" false ;
  check "file = \"/some/file.ml\"" true ;
  check "file <> \"/some/file.ml\"" false ;
  check "file = file.ml" false ;
  check "file <> file.ml" true ;
  check "file =~ file.ml" true ;
  check "file =~! file.ml" false ;
  check "title = \"some title\"" true ;
  check "title <> \"some title\"" false ;
  check "some" true ;
  check "tags" true ;
  check "nope" false ;
  (* Operator: negation. *)
  check "not true" false ;
  check "not false" true ;
  check "not file = \"/some/file.ml\"" false ;
  check "not file <> \"/some/file.ml\"" true ;
  check "not file = file.ml" true ;
  check "not file <> file.ml" false ;
  check "not file =~ file.ml" false ;
  check "not file =~! file.ml" true ;
  check "not title = \"some title\"" false ;
  check "not title <> \"some title\"" true ;
  check "/some" false ;
  check "/tags" false ;
  check "/nope" true ;
  check "not some" false ;
  check "not tags" false ;
  check "not nope" true ;
  (* Operators: conjunction and disjunction. *)
  check "file = file.ml || file = \"/some/file.ml\"" true ;
  check "file = file.ml && file = \"/some/file.ml\"" false ;
  check "some && file =~ file" true ;
  check "nope && file =~ file" false ;
  check "some || file =~ file" true ;
  check "nope || file =~ file" true ;
  check "nope || file =~! file" false ;
  check "file =~ file && not (title =~ title || some) && /nope" false ;
  check "file =~ file && (title =~ title || some) && /nope" true ;
  check "file =~ file && not (title =~ title || some) && nope" false ;
  check "file = file && not (title =~ title || some) && /nope" false ;
  unit

let () =
  Test.register
    ~__FILE__
    ~title:"TSL parser: random roundtrips"
    ~tags:["tsl"; "parser"; "roundtrip"]
    ~seed:Random
  @@ fun () ->
  for _ = 1 to 100000 do
    let random_string () =
      if Random.bool () then
        String.init (Random.int 10) (fun _ ->
            Char.chr (Char.code 'a' + Random.int 26))
      else if Random.bool () then
        String.init (Random.int 10) (fun _ -> Char.chr (Random.int 256))
      else String.init (Random.int 200) (fun _ -> Char.chr (Random.int 256))
    in
    let rec random depth : TSL_AST.t =
      match Random.int (if depth > 0 then 7 else 2) with
      | 0 -> True
      | 1 -> False
      | 2 ->
          String_predicate
            ( (if Random.bool () then File else Title),
              if Random.bool () then Is (random_string ())
              else Matches (rex "let's not try to generate random regexps") )
      | 3 -> Has_tag (random_string ())
      | 4 -> Not (random (depth - 1))
      | 5 -> And (random (depth - 1), random (depth - 1))
      | _ -> Or (random (depth - 1), random (depth - 1))
    in
    let tsl = random (Random.int 5) in
    let string = TSL.show ~always_parenthesize:true tsl in
    let result = parse string in
    if result <> tsl then
      Test.fail
        "%S does not parse as expected but as: %s"
        string
        (TSL.show result)
  done ;
  unit

let () =
  Test.register
    ~__FILE__
    ~title:"TSL: conjunction helper"
    ~tags:["tsl"; "conjunction"]
  @@ fun () ->
  let check items expected =
    let tsl = TSL.conjunction items in
    if tsl <> expected then
      Test.fail "expected %s, got %s" (TSL.show expected) (TSL.show tsl)
  in
  check [] True ;
  check [False] False ;
  check
    [True; False; Has_tag "a"; Has_tag "hello"]
    (And (And (And (True, False), Has_tag "a"), Has_tag "hello")) ;
  unit

let () =
  Test.register
    ~__FILE__
    ~title:"TSL: is_valid_tag"
    ~tags:["tsl"; "is_valid_tag"]
  @@ fun () ->
  let check tag expected =
    let result = TSL.is_valid_tag tag in
    if result <> expected then
      Test.fail "expected is_valid_tag %S = %b, got %b" tag expected result
  in
  (* Some tags are in fact valid. *)
  check "this_is_a_typical_valid_tag_42" true ;
  check "regression" true ;
  (* There are restrictions on the length of tags. *)
  check "" false ;
  check "0" true ;
  check "01234567890123456789012345678901" true ;
  check "012345678901234567890123456789012" false ;
  (* Some keywords cannot be tags. *)
  check "true" false ;
  check "false" false ;
  (* But some can. *)
  check "not" true ;
  (* Some characters cannot be used in tags. *)
  check "Capitalized" false ;
  check "UPPERCASE" false ;
  check "random_Case" false ;
  check "space character" false ;
  check "/slash" false ;
  check "dot.ml" false ;
  check "dash-character" false ;
  unit

let () =
  Test.register ~__FILE__ ~title:"TSL: extract tags" ~tags:["tsl"; "tags"]
  @@ fun () ->
  let check string_to_parse expected_tags =
    Check.((TSL.tags (parse string_to_parse) = expected_tags) (list string))
      ~error_msg:"expected %R, got %L"
  in
  check "true" [] ;
  check "hello && /bye && file = truc.ml" ["bye"; "hello"] ;
  unit
