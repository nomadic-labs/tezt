(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* [Tezt] and its submodule [Base] are designed to be opened.
   [Tezt] is the main module of the library and it only contains submodules,
   such as [Test] which is used below.
   [Tezt.Base] contains values such as [unit] which is used below. *)
open Tezt_core
open Base

(* Register as many tests as you want like this. *)
let () =
  Test.register
  (* [~__FILE__] contains the name of the file in which the test is defined.
     It allows to select tests to run based on their filename. *)
    ~__FILE__
      (* Titles uniquely identify tests so that they can be individually selected. *)
    ~title:"demo"
      (* Tags are another way to group tests together to select them. *)
    ~tags:["math"; "addition"]
  @@ fun () ->
  (* Here is the actual test. *)
  if 1 + 1 <> 2 then Test.fail "expected 1 + 1 = 2, got %d" (1 + 1) ;
  (* Here is another way to write the same test. *)
  Check.((1 + 1 = 2) int) ~error_msg:"expected 1 + 1 = %R, got %L" ;
  Log.info "Math is safe today." ;
  (* [unit] is [Lwt.return ()]. *)
  unit

let () =
  Test.register
    ~__FILE__
    ~title:"fixed seed"
    ~tags:["seed"; "fixed"]
    ~seed:(Fixed 1234)
  @@ fun () ->
  if Test.current_test_seed_specification () <> Fixed 1234 then
    Test.fail "expected current_test_seed_specification = Fixed 1234" ;
  Check.((Test.current_test_seed () = 1234) int)
    ~error_msg:"expected seed = %R, got %L" ;
  let x = Random.int64 Int64.max_int in
  (* 7949467250670036511 is for OCaml 5, 3284848139645624427 is for earlier versions. *)
  if x <> 3284848139645624427L && x <> 7949467250670036511L then
    Test.fail
      "expected x = 3284848139645624427 or 7949467250670036511, got %Ld"
      x ;
  unit

let () =
  Test.register
    ~__FILE__
    ~title:"random seed"
    ~tags:["seed"; "random"]
    ~seed:Random
  @@ fun () ->
  if Test.current_test_seed_specification () <> Random then
    Test.fail "expected current_test_seed_specification = Random" ;
  let x = Random.int64 Int64.max_int in
  Check.((x <> 2497643567980153264L) int64)
    ~error_msg:
      "expected x <> %R, got %L (there is a 1/2^63 chance that this happens)" ;
  unit

let () =
  Test.register
    ~__FILE__
    ~title:"string_tree: mem_prefix_of"
    ~tags:["string_tree"]
  @@ fun () ->
  let split file = String.split_on_char '/' file |> List.rev in
  List.iter
    (fun (suffixes, tests) ->
      let suffix_tree =
        List.fold_left
          (fun tree s -> Test.String_tree.add (split s) tree)
          Test.String_tree.empty
          suffixes
      in
      List.iter
        (fun (file, expected_res) ->
          let res = Test.String_tree.mem_prefix_of (split file) suffix_tree in
          Check.(
            (res = expected_res)
              bool
              ~__LOC__
              ~error_msg:
                (sf "mem_prefix_of(%s, %s) = " file (String.concat ";" suffixes)
                ^ "%L, expected %R")) ;
          Log.info
            "mem_prefix_of(%s, %s) = %b"
            file
            (String.concat ";" suffixes)
            res)
        tests)
    [
      (["c.ml"], [("c.ml", true); ("b/c.ml", true); ("d.ml", false)]);
      ( ["b/c.ml"],
        [("c.ml", false); ("b/c.ml", true); ("d.ml", false); ("a/b/c.ml", true)]
      );
      ([], [("c.ml", false); ("b/c.ml", false)]);
      ( ["b/x.ml"; "c/x.ml"; "y.ml"],
        [
          ("b/x.ml", true);
          ("c/x.ml", true);
          ("y.ml", true);
          ("z/x.ml", false);
          ("z/b/x.ml", true);
        ] );
    ] ;
  unit

let () =
  Test.register ~__FILE__ ~title:"Test.current_test_tags" ~tags:["tag1"; "tag2"]
  @@ fun () ->
  Check.((Test.current_test_tags () = ["tag1"; "tag2"]) (list string))
    ~error_msg:"expected current_test_tags = %R, got %L" ;
  if not (Test.current_test_has_tag "tag1") then
    Test.fail "expected current test to have tag: tag1" ;
  if not (Test.current_test_has_tag "tag2") then
    Test.fail "expected current test to have tag: tag2" ;
  if Test.current_test_has_tag "tag3" then
    Test.fail "did not expect current test to have tag: tag3" ;
  unit
