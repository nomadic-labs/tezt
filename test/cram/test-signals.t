Test various signal sequences.

  $ ./tezt-signal.sh
  Starting test: sleep one second without cooperating
  [SUCCESS] (1/1) sleep one second without cooperating
  $ ./tezt-signal.sh INT
  Starting test: sleep one second without cooperating
  Received SIGINT.
  [ABORTED] (1/1) sleep one second without cooperating
  $ ./tezt-signal.sh TERM
  Starting test: sleep one second without cooperating
  Received SIGTERM.
  [ABORTED] (1/1) sleep one second without cooperating
  $ ./tezt-signal.sh INT INT
  Starting test: sleep one second without cooperating
  Received SIGINT.
  $ ./tezt-signal.sh INT TERM
  Starting test: sleep one second without cooperating
  Received SIGINT.
  Received SIGTERM.
  [ABORTED] (1/1) sleep one second without cooperating
  $ ./tezt-signal.sh TERM INT
  Starting test: sleep one second without cooperating
  Received SIGTERM.
  Received SIGINT.
  [ABORTED] (1/1) sleep one second without cooperating
  $ ./tezt-signal.sh TERM TERM
  Starting test: sleep one second without cooperating
  Received SIGTERM.
  Received SIGTERM.
  [ABORTED] (1/1) sleep one second without cooperating
  $ ./tezt-signal.sh INT INT INT
  Starting test: sleep one second without cooperating
  Received SIGINT.
  ./tezt-signal.sh: 9: kill: No such process
  
  [1]
  $ ./tezt-signal.sh INT INT TERM
  Starting test: sleep one second without cooperating
  Received SIGINT.
  ./tezt-signal.sh: 9: kill: No such process
  
  [1]
  $ ./tezt-signal.sh INT TERM INT
  Starting test: sleep one second without cooperating
  Received SIGINT.
  Received SIGTERM.
  $ ./tezt-signal.sh INT TERM TERM | grep -v ABORTED
  Starting test: sleep one second without cooperating
  Received SIGINT.
  Received SIGTERM.
  Received SIGTERM.
  $ ./tezt-signal.sh TERM TERM INT
  Starting test: sleep one second without cooperating
  Received SIGTERM.
  Received SIGTERM.
  Received SIGINT.

The TERM TERM TERM case is flaky in the CI:
locally it prints three "Received SIGTERM.", while in the CI,
sometimes the third "Received SIGTERM." is not printed.
So we remove it.
