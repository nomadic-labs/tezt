Run a test that should succeed:

  $ ./tezt.sh --file main.ml --test 'Success'
  [SUCCESS] (1/1) Success

Run a test that should fail:

  $ ./tezt.sh --file main.ml --test 'Failing test'
  Starting test: Failing test
  [error] Always failing test
  [FAILURE] (1/1, 1 failed) Failing test
  Try again with: _build/default/main.exe --verbose --file test/cram/main.ml --title 'Failing test'
  [1]

Without --keep-going:

  $ ./tezt.sh --file main.ml --test 'Failing test' --test 'Success' --test 'Success 2'
  [SUCCESS] (1/3) Success
  Starting test: Failing test
  [error] Always failing test
  [FAILURE] (2/3, 1 failed) Failing test
  Try again with: _build/default/main.exe --verbose --file test/cram/main.ml --title 'Failing test'
  [1]

With --keep-going:

  $ ./tezt.sh --file main.ml --test 'Failing test' --test 'Success' --test 'Success 2' -k
  [SUCCESS] (1/3) Success
  Starting test: Failing test
  [error] Always failing test
  [FAILURE] (2/3, 1 failed) Failing test
  Try again with: _build/default/main.exe --verbose --file test/cram/main.ml --title 'Failing test'
  [SUCCESS] (3/3, 1 failed) Success 2
  [1]
