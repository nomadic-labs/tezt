Test --warn-after-timeout:

  $ ./tezt.sh --title 'sleep one second' --warn-after-timeout 0.75 -j 2
  [warn] Test is still running: "sleep one second"
  [SUCCESS] (1/1) sleep one second

Warnings repeat until the test ends:

  $ ./tezt.sh --title 'sleep one second' --warn-after-timeout 0.4 -j 2
  [warn] Test is still running: "sleep one second"
  [warn] Test is still running: "sleep one second"
  [SUCCESS] (1/1) sleep one second

Without -j, this has no effect for now:

  $ ./tezt.sh --title 'sleep one second' --warn-after-timeout 0.75
  [SUCCESS] (1/1) sleep one second

A null value disables the warning:

  $ ./tezt.sh --title 'sleep one second' --warn-after-timeout 0 -j 2
  [SUCCESS] (1/1) sleep one second

Negative values disable the warning:

  $ ./tezt.sh --title 'sleep one second' --warn-after-timeout '\-0.75' -j 2
  [SUCCESS] (1/1) sleep one second

Example with two concurrent tests:

  $ ./tezt.sh --title 'sleep one second' --title 'sleep half a second' --warn-after-timeout 0.4 -j 2
  [warn] Test is still running: "sleep one second"
  [warn] Test is still running: "sleep half a second"
  [SUCCESS] (2/2) sleep half a second
  [warn] Test is still running: "sleep one second"
  [SUCCESS] (1/2) sleep one second

If a test is not cooperating, the regular timeout has no effect:

  $ ./tezt.sh --title 'sleep one second without cooperating' --test-timeout 0.5 -j 2
  [error] Sending SIGTERM to test which is taking too long: "sleep one second without cooperating"
  [ABORTED] (1/1) sleep one second without cooperating
  [2]

But one can force it to die:

  $ ./tezt.sh --title 'sleep one second without cooperating' --test-timeout 0.5 --cleanup-timeout 0.2 -j 2
  [error] Sending SIGTERM to test which is taking too long: "sleep one second without cooperating"
  [error] Sending SIGKILL to test which is taking too long to stop: "sleep one second without cooperating"
  [error] worker was killed by SIGKILL
  [FAILURE] (1/1, 1 failed) sleep one second without cooperating
  Try again with: _build/default/main.exe --verbose --file test/cram/main.ml --title 'sleep one second without cooperating'
  [1]

Workers can also be not very cooperative when they are supposed to exit.

  $ ./tezt.sh --title 'sleep one second at exit' -j 2
  [SUCCESS] (1/1) sleep one second at exit
  [warn] Worker exited with code 42 while not running a test
  $ ./tezt.sh --title 'sleep one second at exit' -j 2 --cleanup-timeout 0.5
  [SUCCESS] (1/1) sleep one second at exit
  [warn] Worker was killed by SIGKILL while not running a test
  $ ./tezt.sh --title 'sleep one second at exit' -j 2 --cleanup-timeout 0.5 --verbose
  Starting test: sleep one second at exit
  [SUCCESS] (1/1) sleep one second at exit
  Send SIGKILL to worker which is still running despite not running any test
  [warn] Worker was killed by SIGKILL while not running a test
