#!/bin/sh

dune build ./main.exe 2>&1
./main.exe --no-log-timestamp -v -t 'sleep one second without cooperating' 2>&1 &
pid=$!

for sig in "$@"; do
  sleep 0.1
  kill "-$sig" "$pid"
done
