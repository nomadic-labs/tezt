Test the '--file' filter.

  $ ./tezt.sh selection --list
  +----------+----------+-----------+
  |   FILE   |  TITLE   |   TAGS    |
  +----------+----------+-----------+
  | a/b/c.ml | a/b/c.ml | selection |
  | a/b/g.ml | a/b/g.ml | selection |
  | a/c.ml   | a/c.ml   | selection |
  | d.ml     | d.ml     | selection |
  | e.ml     | e.ml     | selection |
  +----------+----------+-----------+

  $ ./tezt.sh selection --file 'a/b/c.ml' --list
  +----------+----------+-----------+
  |   FILE   |  TITLE   |   TAGS    |
  +----------+----------+-----------+
  | a/b/c.ml | a/b/c.ml | selection |
  +----------+----------+-----------+
  $ ./tezt.sh selection --file 'c.ml' --list
  +----------+----------+-----------+
  |   FILE   |  TITLE   |   TAGS    |
  +----------+----------+-----------+
  | a/b/c.ml | a/b/c.ml | selection |
  | a/c.ml   | a/c.ml   | selection |
  +----------+----------+-----------+
  $ ./tezt.sh selection --file 'b/c.ml' --list
  +----------+----------+-----------+
  |   FILE   |  TITLE   |   TAGS    |
  +----------+----------+-----------+
  | a/b/c.ml | a/b/c.ml | selection |
  +----------+----------+-----------+
  $ ./tezt.sh selection --file 'a/c.ml' --list
  +--------+--------+-----------+
  |  FILE  | TITLE  |   TAGS    |
  +--------+--------+-----------+
  | a/c.ml | a/c.ml | selection |
  +--------+--------+-----------+
  $ ./tezt.sh selection --file 'd.ml' --list
  +------+-------+-----------+
  | FILE | TITLE |   TAGS    |
  +------+-------+-----------+
  | d.ml | d.ml  | selection |
  +------+-------+-----------+
  $ ./tezt.sh selection --file '' --list
  [warn] Unknown file or file suffix: 
  No test found for filters: --file  selection
  [3]
  $ ./tezt.sh selection --file '.ml' --list
  [warn] Unknown file or file suffix: .ml
  No test found for filters: --file .ml selection
  [3]
  $ ./tezt.sh selection --file 'c.ml' --file 'd.ml' --list
  +----------+----------+-----------+
  |   FILE   |  TITLE   |   TAGS    |
  +----------+----------+-----------+
  | a/b/c.ml | a/b/c.ml | selection |
  | a/c.ml   | a/c.ml   | selection |
  | d.ml     | d.ml     | selection |
  +----------+----------+-----------+
  $ ./tezt.sh selection --not-file 'c.ml' --list
  +----------+----------+-----------+
  |   FILE   |  TITLE   |   TAGS    |
  +----------+----------+-----------+
  | a/b/g.ml | a/b/g.ml | selection |
  | d.ml     | d.ml     | selection |
  | e.ml     | e.ml     | selection |
  +----------+----------+-----------+
  $ ./tezt.sh selection --not-file 'b/g.ml' --list
  +----------+----------+-----------+
  |   FILE   |  TITLE   |   TAGS    |
  +----------+----------+-----------+
  | a/b/c.ml | a/b/c.ml | selection |
  | a/c.ml   | a/c.ml   | selection |
  | d.ml     | d.ml     | selection |
  | e.ml     | e.ml     | selection |
  +----------+----------+-----------+
  $ ./tezt.sh selection --file 'c.ml' --not-file 'b/c.ml' --list
  +--------+--------+-----------+
  |  FILE  | TITLE  |   TAGS    |
  +--------+--------+-----------+
  | a/c.ml | a/c.ml | selection |
  +--------+--------+-----------+

With TSL, paths are strict.

  $ ./tezt.sh 'file = c.ml'
  No test found for filters: file = c.ml
  You can use --list to get the list of tests and their tags.
  [3]

But one can get a behavior that is close to the one of --file with regexps.

  $ ./tezt.sh 'file =~ "/c.ml$"'
  [SUCCESS] (1/2) a/b/c.ml
  [SUCCESS] (2/2) a/c.ml

Some expressions cannot be expressed without TSL.

  $ ./tezt.sh 'file = test/cram/main.ml && success || file =~ c.ml' --list
  +-------------------+-----------+------------------------+
  |       FILE        |   TITLE   |          TAGS          |
  +-------------------+-----------+------------------------+
  | test/cram/main.ml | Success   | retry, success, resume |
  | test/cram/main.ml | Success 2 | success, resume        |
  | a/b/c.ml          | a/b/c.ml  | selection              |
  | a/c.ml            | a/c.ml    | selection              |
  +-------------------+-----------+------------------------+

Multiple arguments result in a conjunction.

  $ ./tezt.sh 'file = test/cram/main.ml && success' 'retry || selection' --list
  +-------------------+---------+------------------------+
  |       FILE        |  TITLE  |          TAGS          |
  +-------------------+---------+------------------------+
  | test/cram/main.ml | Success | retry, success, resume |
  +-------------------+---------+------------------------+

Selecting an empty list of tests.

  $ ./tezt.sh math check
  [warn] Unknown tag: check
  [warn] Unknown tag: math
  No test found for filters: math && check
  You can use --list to get the list of tests and their tags.
  [3]
  $ ./tezt.sh math check --on-empty-test-list ignore
  [warn] Unknown tag: check
  [warn] Unknown tag: math
  $ ./tezt.sh math check --on-empty-test-list warn
  [warn] Unknown tag: check
  [warn] Unknown tag: math
  No test found for filters: math && check
  You can use --list to get the list of tests and their tags.
  $ ./tezt.sh math check --on-empty-test-list fail
  [warn] Unknown tag: check
  [warn] Unknown tag: math
  No test found for filters: math && check
  You can use --list to get the list of tests and their tags.
  [3]

Passing invalid regular expressions results in an error.

  $ ./tezt.sh --match "*hello"
  Error: invalid Perl regular expression: *hello
  See --help.
  [1]
  $ ./tezt.sh 'file =~ "*hello"'
  Error: invalid TSL expression: file =~ "*hello"
  See --help.
  [1]
