One can define and pass custom arguments.

  $ ./tezt.sh --test clap -i
  Starting test: clap
  Custom arg is default.
  [SUCCESS] (1/1) clap
  $ ./tezt.sh --test clap --custom-arg hello -i
  Starting test: clap
  Custom arg is hello.
  [SUCCESS] (1/1) clap

It even appears in --help.

  $ ./tezt.sh --help | grep -A 1 'custom-arg VALUE'
      --custom-arg VALUE (default: default)
          A custom argument for a specific test.

There was a bug where Cli.Selecting_tests.tsl_expression would not return the same
result if it was called multiple times.
With this bug, the following would return 'true'.

  $ ./tezt.sh 'tsl && title = "Cli.Selecting_tests.tsl_expression"' -i
  Starting test: Cli.Selecting_tests.tsl_expression
  TSL = tsl && title = Cli.Selecting_tests.tsl_expression
  [SUCCESS] (1/1) Cli.Selecting_tests.tsl_expression
