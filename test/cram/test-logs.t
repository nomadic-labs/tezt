Default logs only show errors, but one can show more.

  $ ./tezt.sh --file main.ml --title Success --info
  Starting test: Success
  Success test.
  [SUCCESS] (1/1) Success
  $ ./tezt.sh --file main.ml --title Success --verbose
  Starting test: Success
  Success test.
  This is a verbose log.
  [SUCCESS] (1/1) Success

Logging at exit works for standard output.

  $ ./tezt.sh --file main.ml --log-file tmp-log-file-of-cram-test.log at_exit --info
  Starting test: log at exit
  [SUCCESS] (1/1) log at exit
  Normally-registered at_exit.
  [warn] Cannot log "Spaghetti-registered at_exit." to file: file is closed. Did you try to log from an at_exit handler that is registered before the Log module? None of the following log messages will appear in the log file.
  Spaghetti-registered at_exit.
  Another spaghetti-registered at_exit.

In log files though, we only see logs from at_exit callbacks if those at_exit were
registered *after* the at_exit in the Log module.

  $ cut -b 1- tmp-log-file-of-cram-test.log
  Starting test: log at exit
  [SUCCESS] (1/1) log at exit
  Normally-registered at_exit.

Log prefixes and timestamps can be configured.
In the following test we hide "exited with code 0" log messages because they can appear
before or after the standard output of the corresponding process, making this test flaky.

  $ ./tezt.sh "log && prefix || success" -v --log-timestamp | sed 's/\[[[:digit:]:\.]\+\] /[TIMESTAMP] /' | grep -v 'exited with code 0'
  [TIMESTAMP] Starting test: log without prefix
  [TIMESTAMP] Timestamps are printed by default: true
  [TIMESTAMP] Prefixes are printed by default: true
  [TIMESTAMP] Log is printed with default configuration
  [TIMESTAMP] [echo#0] echo 'Log is printed following the default configuration'
  [TIMESTAMP] [echo#0] Log is printed following the default configuration
  There is no timestamp here
  echo 'There is no timestamp nor the name of the command here'
  There is no timestamp nor the name of the command here
  [TIMESTAMP] [SUCCESS] (1/3) log without prefix
  [TIMESTAMP] Starting test: Success
  [TIMESTAMP] Success test.
  [TIMESTAMP] This is a verbose log.
  [TIMESTAMP] [SUCCESS] (2/3) Success
  [TIMESTAMP] Starting test: Success 2
  [TIMESTAMP] Another successful test.
  [TIMESTAMP] [SUCCESS] (3/3) Success 2

Check CLI option  `no-log-timestamp`

  $ ./tezt.sh log prefix -v --no-log-timestamp | grep -v 'exited with code 0'
  Starting test: log without prefix
  Timestamps are printed by default: false
  Prefixes are printed by default: true
  Log is printed with default configuration
  [echo#0] echo 'Log is printed following the default configuration'
  [echo#0] Log is printed following the default configuration
  There is no timestamp here
  echo 'There is no timestamp nor the name of the command here'
  There is no timestamp nor the name of the command here
  [SUCCESS] (1/1) log without prefix

Check CLI option `no-log-prefix` `--log-timestamp`

  $ ./tezt.sh log prefix -v --no-log-prefix --log-timestamp | sed 's/\[[[:digit:]:\.]\+\] /[TIMESTAMP] /' | grep -v 'exited with code 0'
  [TIMESTAMP] Starting test: log without prefix
  [TIMESTAMP] Timestamps are printed by default: true
  [TIMESTAMP] Prefixes are printed by default: false
  [TIMESTAMP] Log is printed with default configuration
  [TIMESTAMP] echo 'Log is printed following the default configuration'
  [TIMESTAMP] Log is printed following the default configuration
  There is no timestamp here
  echo 'There is no timestamp nor the name of the command here'
  There is no timestamp nor the name of the command here
  [TIMESTAMP] (1/1) log without prefix

Check both CLI options `no-log-prefix` and `no-log-timestamp`

  $ ./tezt.sh log prefix -v --no-log-prefix  --no-log-timestamp | grep -v 'exited with code 0'
  Starting test: log without prefix
  Timestamps are printed by default: false
  Prefixes are printed by default: false
  Log is printed with default configuration
  echo 'Log is printed following the default configuration'
  Log is printed following the default configuration
  There is no timestamp here
  echo 'There is no timestamp nor the name of the command here'
  There is no timestamp nor the name of the command here
  (1/1) log without prefix

Test Log.clear_error_context_queue

  $ ./tezt.sh -t 'Log.clear_error_context_queue'
  This will be printed.
  [error] This should print the log queue, i.e. 'This will be printed' above.
  [SUCCESS] (1/1) Log.clear_error_context_queue
