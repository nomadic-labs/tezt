(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021-2022 Nomadic Labs <contact@nomadic-labs.com>           *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* The following tests are not meant to be executed directly. They are
   tools used in cram scripts (see the [.t] files in this folder) to
   test the behavior of Tezt itself. *)

let () =
  Test.register ~__FILE__ ~title:"log without prefix" ~tags:["log"; "prefix"]
  @@ fun () ->
  Log.info "Timestamps are printed by default: %b" Cli.Logs.timestamp ;
  Log.info "Prefixes are printed by default: %b" Cli.Logs.prefix ;
  let () = Log.info "Log is printed with default configuration" in
  let* () =
    Process.run "echo" ["Log is printed following the default configuration"]
  in
  Log.Style.set_prefix Hidden ;
  Log.Style.set_timestamp Hidden ;
  Log.info "There is no timestamp here" ;
  Process.run "echo" ["There is no timestamp nor the name of the command here"]

let () =
  Test.register ~__FILE__ ~title:"Success" ~tags:["retry"; "success"; "resume"]
  @@ fun () ->
  Log.info "Success test." ;
  Log.debug "This is a verbose log." ;
  unit

let () =
  let should_fail = ref true in
  Test.register
    ~__FILE__
    ~title:"Fail every other run test"
    ~tags:["retry"; "fail"; "flake"]
  @@ fun () ->
  if !should_fail then (
    should_fail := false ;
    Test.fail "Failing test on first try")
  else (
    should_fail := true ;
    Log.info "Works on second" ;
    unit)

let () =
  Test.register
    ~__FILE__
    ~title:"Failing test"
    ~tags:["retry"; "fail"; "always"; "resume"]
  @@ fun () -> Test.fail "Always failing test"

(* This one is registered after the failing test, to test --keep-going. *)
let () =
  Test.register ~__FILE__ ~title:"Success 2" ~tags:["success"; "resume"]
  @@ fun () ->
  Log.info "Another successful test." ;
  unit

(* Used to test selection of tests *)
let () =
  let files = ["a/b/c.ml"; "a/b/g.ml"; "a/c.ml"; "d.ml"; "e.ml"] in
  List.iter
    (fun file ->
      Test.register ~__FILE__:file ~title:file ~tags:["selection"] (fun () ->
          unit))
    files

let () =
  Test.register ~__FILE__ ~title:"Cli.get" ~tags:["cli"; "options"] @@ fun () ->
  let ucase s = Some (String.uppercase_ascii s) in
  let option_to_string to_string = function
    | None -> "None"
    | Some s -> sf "Some %s" (to_string s)
  in
  Log.info
    "str_ucase: %s"
    (option_to_string Fun.id (Cli.get_opt ucase "str_ucase")) ;
  Log.info "int: %s" (option_to_string string_of_int (Cli.get_int_opt "int")) ;
  Log.info
    "bool: %s"
    (option_to_string string_of_bool (Cli.get_bool_opt "bool")) ;
  Log.info
    "float: %s"
    (option_to_string string_of_float (Cli.get_float_opt "float")) ;
  unit

(* Used to test job selection *)
let () =
  let titles = ["4s test"; "2s test (1)"; "2s test (2)"] in
  List.iter
    (fun title ->
      Test.register ~__FILE__ ~title ~tags:["job_selection"] (fun () -> unit))
    titles

let log_something_on_cleanup = ref false

let () =
  Test.declare_clean_up_function @@ fun test_result ->
  if !log_something_on_cleanup then (
    log_something_on_cleanup := false ;
    let test_result =
      match test_result with
      | Successful -> "Successful"
      | Failed _ -> "Failed"
      | Aborted -> "Aborted"
    in
    Log.info
      "Clean up hook was triggered for test %S from %s (test_result = %s)."
      (Test.current_test_title ())
      (Test.current_test_file ())
      test_result)

let raise_on_cleanup = ref false

let () =
  Test.declare_clean_up_function @@ fun _ ->
  if !raise_on_cleanup then (
    raise_on_cleanup := false ;
    failwith "Clean up hook was triggered.")

let () =
  Test.register ~__FILE__ ~title:"Clean up hook: nothing" ~tags:["cleanup"]
  @@ fun () -> unit

let () =
  Test.register ~__FILE__ ~title:"Clean up hook: log" ~tags:["cleanup"]
  @@ fun () ->
  log_something_on_cleanup := true ;
  unit

let () =
  Test.register ~__FILE__ ~title:"Clean up hook: fail and log" ~tags:["cleanup"]
  @@ fun () ->
  log_something_on_cleanup := true ;
  Test.fail "failing on purpose"

let () =
  Test.register ~__FILE__ ~title:"Clean up hook: raise" ~tags:["cleanup"]
  @@ fun () ->
  raise_on_cleanup := true ;
  unit

let () =
  let custom_arg =
    Clap.default_string
      ~description:"A custom argument for a specific test."
      ~long:"custom-arg"
      "default"
  in
  Test.register ~__FILE__ ~title:"clap" ~tags:["clap"] @@ fun () ->
  Log.info "Custom arg is %s." custom_arg ;
  unit

let () =
  Test.register ~__FILE__ ~title:"log at exit" ~tags:["at_exit"] @@ fun () ->
  (at_exit @@ fun () -> Log.info "Normally-registered at_exit.") ;
  (Spaghetti_exit.callback :=
     fun () ->
       Log.info "Spaghetti-registered at_exit." ;
       Log.info "Another spaghetti-registered at_exit.") ;
  unit

let () =
  Test.register
    ~__FILE__
    ~title:"Cli.Selecting_tests.tsl_expression"
    ~tags:["tsl"]
  @@ fun () ->
  Log.info
    "TSL = %s"
    (Tezt_core.TSL.show (Cli.Selecting_tests.tsl_expression ())) ;
  unit

let () =
  Test.register ~__FILE__ ~title:"sleep one second" ~tags:["sleep"; "nolist"]
  @@ fun () -> Lwt_unix.sleep 1.

let () =
  Test.register
    ~__FILE__
    ~title:"sleep one second without cooperating"
    ~tags:["sleep"; "nolist"]
  @@ fun () ->
  Unix.sleep 1 ;
  unit

let () =
  Test.register ~__FILE__ ~title:"sleep half a second" ~tags:["sleep"; "nolist"]
  @@ fun () -> Lwt_unix.sleep 0.5

let () =
  Test.register
    ~__FILE__
    ~title:"Log.clear_error_context_queue"
    ~tags:["nolist"]
  @@ fun () ->
  Log.info "This will not be printed." ;
  Log.clear_error_context_queue () ;
  Log.info "This will be printed." ;
  Log.error
    "This should print the log queue, i.e. 'This will be printed' above." ;
  unit

let () =
  Test.register
    ~__FILE__
    ~title:"sleep one second at exit"
    ~tags:["sleep"; "nolist"]
  @@ fun () ->
  at_exit (fun () ->
      Unix.sleep 1 ;
      exit 42) ;
  unit

let () = Test.run ()
